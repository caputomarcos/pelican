Title: Compilation des liens de la semaine 2017-01-22
Date:  2017-01-22
Tags:  veille, spark, stream, docker, machinelearning, architecture, mesos, kubernetes, management, scala, websocket, akka, fastdata
Category:  veille
Compilation des [liens](https://stream.logpickr.com) de la semaine


# [Spark](https://stream.logpickr.com/tag/spark)
Real-time Streaming ETL with Structured Streaming in Apache Spark 2.1: Part 1 of Scalable data @ Databricks
[https://databricks.com/blog/2017/01/19/real-time-streaming-etl-structured-streaming-apache-spark-2-1.html](https://databricks.com/blog/2017/01/19/real-time-streaming-etl-structured-streaming-apache-spark-2-1.html)  _[#stream](https://stream.logpickr.com/tag/stream)_  

Spark and Kerberos: a safe story - Stratio Blog
[http://blog.stratio.com/spark-kerberos-safe-story](http://blog.stratio.com/spark-kerberos-safe-story)



# [Streaming et Fast Data](https://stream.logpickr.com/tag/stream)
2017 – Year of FAST Data | Knoldus
[https://blog.knoldus.com/2016/12/31/2017-year-of-fast-data](https://blog.knoldus.com/2016/12/31/2017-year-of-fast-data)  _[#fastdata](https://stream.logpickr.com/tag/fastdata)_  


# [Machine Learning](https://stream.logpickr.com/tag/machinelearning)
Machine Learning Explained: Algorithms Are Your Friend
[http://blog.dataiku.com/machine-learning-explained-algorithms-are-your-friend](http://blog.dataiku.com/machine-learning-explained-algorithms-are-your-friend)






# Autres
Docker 1.13 CLI Management Commands
[http://blog.arungupta.me/docker-1-13-management-commands/](http://blog.arungupta.me/docker-1-13-management-commands/)  _[#docker](https://stream.logpickr.com/tag/docker)_  

La signature d’images Docker sur une Registry avec Notary | OCTO talks !
[http://blog.octo.com/la-signature-dimages-docker-sur-une-registry-avec-notary](http://blog.octo.com/la-signature-dimages-docker-sur-une-registry-avec-notary)  _[#docker](https://stream.logpickr.com/tag/docker)_  

Distributed Fabric: A New Architecture for Container-Based Applications - The New Stack
[http://thenewstack.io/distributed-fabric-new-architecture-container-based-applications](http://thenewstack.io/distributed-fabric-new-architecture-container-based-applications)  _[#architecture](https://stream.logpickr.com/tag/architecture)_    _[#docker](https://stream.logpickr.com/tag/docker)_    _[#mesos](https://stream.logpickr.com/tag/mesos)_    _[#kubernetes](https://stream.logpickr.com/tag/kubernetes)_  

Le Feedback entre Pairs
[https://www.infoq.com/fr/news/2017/01/peer-feedback-tech-teams](https://www.infoq.com/fr/news/2017/01/peer-feedback-tech-teams)  _[#management](https://stream.logpickr.com/tag/management)_  

Use Vault with Consul on Docker | 42notes
[https://42notes.wordpress.com/2015/05/01/use-vault-with-consul-on-docker](https://42notes.wordpress.com/2015/05/01/use-vault-with-consul-on-docker)  _[#docker](https://stream.logpickr.com/tag/docker)_  

Upload File using Akka-HTTP and Binary protocol in Scala through web socket. | Knoldus
[https://blog.knoldus.com/2016/12/30/upload-file-using-akka-http-and-binary-protocol-in-scala-through-web-socket](https://blog.knoldus.com/2016/12/30/upload-file-using-akka-http-and-binary-protocol-in-scala-through-web-socket)  _[#scala](https://stream.logpickr.com/tag/scala)_    _[#websocket](https://stream.logpickr.com/tag/websocket)_    _[#akka](https://stream.logpickr.com/tag/akka)_  





----
_[Source d'inspiration](https://blog.jbfavre.org/2016/08/26/compilation-veille-twitter-rss/)_