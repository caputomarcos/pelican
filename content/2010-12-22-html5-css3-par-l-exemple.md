date: 2010-12-21  
published: true  
tags: collegue, lien, html5, css3   
title: Mastering html5 + css3  

Bien que totalement amateur et pas doué, j’aime bien, au détour d’un
site qui m’a accroché l’oeil, regarder le code source de la page et ses
feuilles de style.  
C’est la force d’html, on a accès au code source !

C’est encore plus sympa d’avoir dans son bureau un vrai pro du sujet,
qui explique simplement ses recettes par des exemples concrets. Enjoy !

<http://debray.jerome.free.fr/>

<http://debray-jerome.developpez.com/>

Pour découvrir plein de choses sur HTML5 et CSS3.

Et pour en avoir plein les yeux: <http://html5advent.com> avec des démos
époustouflantes (mais qui ne fonctionnent pas encore toutes complètement
avec tous les navigateurs … Ca va venir …)

