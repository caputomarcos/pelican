Title:  Création manuelle d'une image docker avec scala et spark
Tags:  scala, spark, docker
Category: bigdata

# Construction manuelle d'une petite image java:alpine + scala + spark
```bash
docker run -ti java:alpine /bin/sh 

apk update
apk add bash
apk add nano

cd
nano ~/.profile

	# ~/.profile: executed by Bourne-compatible login shells.

	if [ -e /bin/bash ]; then
	  if [ -f ~/.bashrc ]; then
	    . ~/.bashrc
	  fi
	fi

	mesg n

nano ~/.bashrc
	SCALA_HOME=/opt/scala-2.11.8
	PATH=$PATH:$SCALA_HOME/bin
	SPARK_HOME=/opt/spark-2.0.0-bin-hadoop2.7
	PATH=$PATH:$SPARK_HOME/bin

mkdir /opt
cd /opt
wget downloads.lightbend.com/scala/2.11.8/scala-2.11.8.tgz
tar xvzf scala-2.11.8.tgz
rm  scala-2.11.8.tgz

wget http://d3kbcqa49mib13.cloudfront.net/spark-2.0.0-bin-hadoop2.7.tgz
tar xvzf spark-2.0.0-bin-hadoop2.7.tgz
rm spark-2.0.0-bin-hadoop2.7.tgz
```

# Sauvegarde de l'image et lancement

```bash
    docker commit 5262e673ffa6 tintouli/spark:alpine
	docker run -ti tintouli/spark:alpine bash -cli spark-shell
```


# Update : génération à partir d'un Dockerfile 

```bash
cat Dockerfile

FROM java:alpine

#use bash
RUN apk update
RUN apk add --no-cache bash

#download and install scala and spark
RUN mkdir /opt
RUN wget -O /opt/scala-2.11.8.tgz downloads.lightbend.com/scala/2.11.8/scala-2.11.8.tgz
RUN tar xvzf /opt/scala-2.11.8.tgz  -C /opt
RUN rm  /opt/scala-2.11.8.tgz

RUN wget http://d3kbcqa49mib13.cloudfront.net/spark-2.0.0-bin-hadoop2.7.tgz
RUN tar xvzf spark-2.0.0-bin-hadoop2.7.tgz  -C /opt
RUN rm spark-2.0.0-bin-hadoop2.7.tgz

# Default to UTF-8 file.encoding
ENV LANG C.UTF-8

# Environment variables
ENV SCALA_HOME /opt/scala-2.11.8
ENV PATH $PATH:$SCALA_HOME/bin
ENV SPARK_HOME /opt/spark-2.0.0-bin-hadoop2.7
ENV PATH $PATH:$SPARK_HOME/bin

```