Title:  Publier sur le Journal du hacker à partir de FreshRSS 
tags:  journalduhacker,freshrss, rss, logiciellibre
catgegory: communauté

Utilisateur régulier de FreshRSS, j'utilise assez régulièrement la fonction de partage, qui permet de publier un lien sur Wallabag, Diaspora, par mail, etc...

Je pensais depuis longtemps : 
>" Ça serait chouette de faire le petit bout de code pour pousser l'article en question sur le Jdh ..."

Et je traînais ça dans ma todolist, dans celle du Journal du hacker
(**oui**, on a ouvert une instance sur [Framaboard](https://journalduhacker.framaboard.org) pour partager les tâches à faire <- **on recrute** ;),  jusqu'à ce que je me décide à regarder un peu sérieusement.

Et en fait c'est très simple :

 *   Éditer le fichier data/shares.php de votre instance FreshRSS (il est auto-documenté)
 *   Ajouter les ligne suivantes avant la dernière parenthèse fermante

```javascript
'Jdh' => array( 
'url' => 'https://www.journalduhacker.net/stories/new?url=~LINK~&title=~TITLE~',
'transform' => array('rawurlencode'),
'form' => 'simple',
),
```
Dans la configuration de FreshRSS (roue dentée > Partage), vous pouvez ajouter un moyen de partage :

L'entrée ajoutée s'appelle gen.share.jdh, je ne sais pas pourquoi, mais vous pouvez lui donner le nom que vous souhaitez, comme pour toutes les extension de partage de FreshRSS ![entrées](https://blog.journalduhacker.net/data/medias/freshrss-share1.png)

Après validation de la modification, vous pouvez retourner à vos flux, et une nouvelle entrée de partage est disponible (raccourci clavier 's') ![partages](https://blog.journalduhacker.net/data/medias/freshrss-share2.png)

C'est plus rapide pour contribuer au [Journal du hacker](https://www.journalduhacker.net/stories/new) !

C'est en contribution chez [FreshRSS](https://github.com/FreshRSS/FreshRSS/pull/1054), en route pour la [version 1.3.1](https://github.com/FreshRSS/FreshRSS/pull/1056) !
