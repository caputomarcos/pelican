date: 2010-12-02  
tags: [ nexus, blog ]  
title: Bonjour, le monde !  

La réunion de ce matin m’a décidé :

Merci [Nicolas](http://www.viadeo.com/fr/profile/nicolas.bulteau) ,
j’ouvre un blog perso, orienté vers tout ce qui est technique, codage,
boulot.

Au programme dans la mesure du temps disponible :

* des langages de programmation (avec de l’histoire dedans …)  
* des méthodes (agiles de préférence)  
* des outils qualité

En ce moment, je suis en pleine réflexion autour des outils production
et qualité (intégration continue, hudson, sonar, maven, nexus)

Au passage, on peut noter que Nexus tourne officiellement depuis cet
après-midi.

Plus sur le sujet bientôt…

