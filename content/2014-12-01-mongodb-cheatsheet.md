Title: Mongo DB cheat sheet
tags: mongodb
Category: bigdata

[MongoDB Manual](http://docs.mongodb.org/manual/)

Client mongoDB multiplateforme (ok sous Ubuntu 14.04) : [Robomongo](http://robomongo.org)

```scala
db.flights.find( {"Origin.airport" : "Livingston Municipal"})

db.flights.find({}, {"Origin.airport": 1 })


db.flights.find({'Carrier.code': {$nin: ['WN', 'US', 'OH', 'OO', 'UA' , 'XE', 'YV']}}, {"_id": 0, "Carrier.description" : 1, "Carrier.code": 1, "Origin.airport": 1})
```
