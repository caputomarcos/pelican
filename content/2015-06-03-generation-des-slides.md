Title: Génération des slides
tags: markdown, pandoc, reveal.js

Un collègue m'a demandé comment je gérais mes [présentations](/slide/bigdata)

Mon trio favori pour cela :

* [reveal.js](http://lab.hakim.se/reveal-js) : librairie javascript dédiée à l'affichage de slides, table des matières, notes orateur, ...
* [pandoc](http://pandoc.org) : outil de conversion multi-formats  (notamment de markdown vers reveal.js)
* [Markdown](http://daringfireball.net/projects/markdown/) : convention de formattage légère des documentations, que l'on commence à voir partout, très pratique pour la prise de notes, la rédaction de billets de blog, + options de coloration syntaxique de code, insertion d'images, tableaux, ....

Le fichier Makefile (qui prend tous les .md d'un répertoire, génère les .html correspondants en reveal.js, et une page d'index :

```bash
all :
make index
for i in *.md; do make `basename $$i .md`.html; done;

%.html : %.md
pandoc  -s  -t revealjs -V theme="moon" --slide-level 2 -o $@ $<

index :
echo "Index" > index.txt;
echo "" >> index.txt;
for i in *.md; do echo "* [`basename $$i .md`.html](`basename $$i .md`.html)  [(raw sources)]($$i)"  >> index.txt; done;
pandoc -f markdown -t html5 -o index.html index.txt
```

Voir les [exemples de Pandoc](http://pandoc.org/demos.html) pour les paramètres utilisés ci-dessus.


__Attention !__

La version de reveal.js utilisée par pandoc n'est pas forcément la dernière de reveal.js, et j'ai eu des problèmes lors des premières générations. Il vaut mieux utiliser la version de librairie reveal.js utilisée par pandoc 

[Ma copie ici](/slide/reveal.js.tgz)
