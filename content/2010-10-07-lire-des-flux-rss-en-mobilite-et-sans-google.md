Title: Lire des flux RSS en mobilité
tags: rss, newsbeuter, debian, mobilite, google, informatique
status : draft

En réponse à [http://blog-marcel.eu/index.php?article43/google-reader-en-console-avec-newsbeuter](.http://blog-marcel.eu/index.php?article43/google-reader-en-console-avec-newsbeuter)

Ton article m'a beaucoup intéressé, car j'ai utilisé intensivement newsbeuter pendant un certain temps, avec la problématique de mobilité (ne serait-ce que boulot-maison).

Je sauvegardais automatiquement le cache newsbeuter sur une clé USB (via l'excellent unison ou autre méthode), mais la synchro était parfois longue, et puis il y a toujours le risque d'oublier, etc...

Ta solution est assez séduisante, mais je voulais aussi me passer de google (autrefois j'avais monté mon instance de gregarius mais la mise à jour de base de donnée chez un hébergeur gratuit ne supportait pas la fréquence d'un aggrégateur de news).

L'autre jour, un peu par hasard en fouillant dans les paquets debian, je suis tombé sur feed2imap. Et là, ça a été la révélation ! Du genre Saint Paul qui tombe de son cheval sur la route de Damas ... D'autant que je n'attendais pas la solution de ce côté :

- une fois configuré avec tes feeds (import de fichier opml à améliorer mais on s'en sort) feed2imap synchronise tes articles avec les dossiers imap de ton choix (un par feed si tu veux )
- diverses options possibles pour chaque feed, y compris la fréquence maximale de mise à jour
- et pour la lecture, donc, n'importe quel client mail, depuis n'importe quelle machine, y compris webmail, avec l'avantage du tri ou de la recherche de ton client mail préféré, la conservation des feeds préférés, le marquage, l'archivage, la redirection, ...
- si tu veux voir l'article complet, tu cliques sur le lien ...

En fait, la gestion des états lu/non lu, marquage, lire plus tard, recherche, filtrage etc ... d'un client mail me semble bien supérieure à tous les clients rss que j'ai pu voir, et a l'avantage de n'utiliser qu'un seul client. D'ailleurs un 

J'utilise depuis deux jours, ça marche du tonnerre, il suffit de régler la fréquence de mise à jour de feed2imap sur ta machine hôte dans sa crontab ...

J'ai beaucoup aimé newsbeuter, mais là, j'avoue, j'en suis encore sur le cul ...

En plus c'est programmé en ruby, ce qui me plait bien (c'est comme ça que je suis tombé dessus en fait).

Enjoy !

----

En fait, en y réfléchissant un peu, c'est équivalent à souscrire un feed dans thunderbird, puis à consulter ses mails en mobilité en imap ou webmail ...

Mais c'est moins fun.

Et puis j'aime bien la séparation mise à jour / lecture des news

et la collaboration de petits outils simples et dédiés à une tâche précise. Ca évite les gros logiciels qui font tout y compris le café...
