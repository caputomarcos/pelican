Title: Compilation des liens de la semaine 2016-10-02
Date:  2016-10-02
Tags:  veille
Category:  bigdata
Compilation des [liens](https://tintouli.withknown.com/tag/bigdata) de la semaine

# Bases de données Graphe
[NoSQL : les trois piliers de la stratégie d’Oracle (pour les gouverner tous)](http://www.lemagit.fr/actualites/450305085/NoSQL-les-trois-piliers-de-la-strategie-dOracle-pour-les-gouverner-tous)  _[#nosql](https://tintouli.withknown.com/tag/nosql)_  

[Gremlin’s Time Machine | DataStax](https://www.datastax.com/dev/blog/gremlins-time-machine)  _[#gremlin](https://tintouli.withknown.com/tag/gremlin)_  

[Time evolving graph processing at scale | the morning paper](https://blog.acolyer.org/2016/09/26/time-evolving-graph-processing-at-scale)  _[#spark](https://tintouli.withknown.com/tag/spark)_  

[Getting Started with DataStax Graph « Tech Notes](https://davefelcey.wordpress.com/2016/09/20/getting-started-with-datastax-graph)





# Machine Learning
[Big Data Processing with Apache Spark - Part 5: Spark ML Data Pipelines](https://www.infoq.com/articles/apache-sparkml-data-pipelines)  _[#spark](https://tintouli.withknown.com/tag/spark)_  






----
_[Source d'inspiration](https://blog.jbfavre.org/2016/08/26/compilation-veille-twitter-rss/)_