Title: Compilation des liens de la semaine 2017-01-15
Date:  2017-01-15
Tags:  veille, machinelearning, java, scala, spark, deeplearning, graph, stream, kafka, supervision, kubernetes
Category:  veille
Compilation des [liens](https://stream.logpickr.com) de la semaine

# [Bases de données Graphe](https://stream.logpickr.com/tag/graph)
The Linux Foundation Welcomes JanusGraph | Linux.com | The source for Linux information
[https://www.linux.com/blog/Linux-Foundation-welcomes-JanusGraph](https://www.linux.com/blog/Linux-Foundation-welcomes-JanusGraph)

JanusGraph: What's new in graph technology - IBM OpenTech
[https://developer.ibm.com/opentech/2017/01/12/janusgraph](https://developer.ibm.com/opentech/2017/01/12/janusgraph)

JanusGraph Picks Up Where TitanDB Left Off
[https://www.datanami.com/2017/01/13/janusgraph-picks-titandb-left-off/](https://www.datanami.com/2017/01/13/janusgraph-picks-titandb-left-off/)


# [Spark](https://stream.logpickr.com/tag/spark)
What 2017 brings for a Java developer?
[http://fruzenshtein.com/what-2017-brings-for-a-java-developer](http://fruzenshtein.com/what-2017-brings-for-a-java-developer)  _[#java](https://stream.logpickr.com/tag/java)_    _[#scala](https://stream.logpickr.com/tag/scala)_  

Intel Open-Sources BigDL, Distributed Deep Learning Library for Apache Spark
[https://www.infoq.com/news/2017/01/bigdl-deep-learning-on-spark](https://www.infoq.com/news/2017/01/bigdl-deep-learning-on-spark)  _[#deeplearning](https://stream.logpickr.com/tag/deeplearning)_  

New codelab: running OpenCV on Google Cloud Dataproc using Apache Spark | Google Cloud Big Data and Machine Learning Blog  |  Google Cloud Platform
[https://cloud.google.com/blog/big-data/2017/01/new-codelab-running-opencv-on-google-cloud-dataproc-using-apache-spark](https://cloud.google.com/blog/big-data/2017/01/new-codelab-running-opencv-on-google-cloud-dataproc-using-apache-spark)



# [Streaming et Fast Data](https://stream.logpickr.com/tag/stream)
Apache Kafka: Getting Started Guide | Confluent
[https://www.confluent.io/blog/apache-kafka-getting-started](https://www.confluent.io/blog/apache-kafka-getting-started)  _[#kafka](https://stream.logpickr.com/tag/kafka)_  


# [Machine Learning](https://stream.logpickr.com/tag/machinelearning)
Survey of Available Machine Learning Frameworks - insideBIGDATA
[http://insidebigdata.com/2017/01/08/16876](http://insidebigdata.com/2017/01/08/16876)





# Autres
Supervision d’applications (2/3) : Collectd et StatsD | BLOG VISEO TECHNOLOGIES
[http://blog.viseo-bt.com/supervision-application-collectd-et-statsd](http://blog.viseo-bt.com/supervision-application-collectd-et-statsd)  _[#supervision](https://stream.logpickr.com/tag/supervision)_  

Starting a Kubernetes 1.5.x cluster
[http://blog.arungupta.me/start-kubernetes-cluster/](http://blog.arungupta.me/start-kubernetes-cluster/)  _[#kubernetes](https://stream.logpickr.com/tag/kubernetes)_  





----
_[Source d'inspiration](https://blog.jbfavre.org/2016/08/26/compilation-veille-twitter-rss/)_