Title: Installation PCDuino 3 nano 
tags: autohebergement, arm

http://www.armbian.com/pcduino3/

http://www.armbian.com/documentation/

# Préparation carte microSD
Insertion, démontage de la carte microSD

```bash
sudo dd bs=1M if=Armbian_4.5_Pcduino3nano_Debian_jessie_3.4.109.raw of=/dev/mmcblk0
sync
```
remontage de la carte, modification de /etc/network/interfaces pour IP fixe
```bash
auto eth0
#iface eth0 inet dhcp
iface eth0 inet static
address 192.168.1.20
netmask 255.255.255.0
gateway 192.168.1.1
```
# Branchement réseau, carte microSD, et alimentation microUSB 5V

Le démarrage prend un peu de temps (2 boots successifs de 3 minutes comme expliqué sur le site)

First boot takes longer (around 3min) than usual (20s) because it updates package list, regenerates SSH keys and expand partition to fit your SD card. It will reboot one time automatically. Second boot also take little longer (around 3min) because it creates 128MB emergency swap space
```bash
nmap -A 192.168.1.20

Starting Nmap 6.40 ( http://nmap.org ) at 2015-12-11 21:11 CET
Nmap scan report for 192.168.1.20
Host is up (0.026s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh OpenSSH 6.7p1 Debian 5 (protocol 2.0)
|_ssh-hostkey: ERROR: Script execution failed (use -d to debug)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
```
Service detection performed. Please report any incorrect results at http://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 1.92 seconds

# Premier boot et upgrade

modification forcée du mot de passe root
```bash
 ____
| __ )  __ _ _ __   __ _ _ __   __ _ 
|  _ \ / _` | '_ \ / _` | '_ \ / _` |
| |_) | (_| | | | | (_| | | | | (_| |
|____/ \__,_|_| |_|\__,_|_| |_|\__,_|
 

Last login: Fri Dec 11 20:52:07 2015 from 192.168.1.13

Load: 1.07, 0.50, 0.24 - Board: 36.2°C - Memory: 928Mb

root@pcduino3nano:~# df
Filesystem 1K-blocksUsed Available Use% Mounted on
/dev/root7640344 1096124   6223916  15% /
devtmpfs  504172   0504172   0% /dev
tmpfs 504284   0504284   0% /dev/shm
tmpfs 504284   13024491260   3% /run
tmpfs   5120   4  51
16   1% /run/lock
tmpfs 504284   0504284   0% /sys/fs/cgroup
tmpfs 262144   0262144   0% /tmp

  root@pcduino3nano:~# uname -a
  
  Linux pcduino3nano 3.4.109-sun7i #4 SMP PREEMPT Sun Oct 11 14:32:15 CEST 2015 armv7l GNU/Linux
  
  root@pcduino3nano:~# more /etc/debian_version 
  8.2
  
  root@pcduino3nano:~# apt update
  
  root@pcduino3nano:~# more /etc/apt/sources.list.d/armbian.list 
deb http://apt.armbian.com jessie main

  apt upgrade
```
# Installation boot et système sur Flash NAND

le script est fourni :

```bash
nand-sata-install
```
des message d'erreur durant l'install ...

```bash
 ┌──────────────────────NAND install────────────────────────┐
 │  │  
 │ All done. Power off and remove SD to boot from NAND  │  
 │  │  
 ├──────────────────────────────────────────────────────────┤  
 │ <Power off>   <  Exit   >│  
 └──────────────────────────────────────────────────────────┘  
```
Mais le reboot sans microSD ne semble pas OK ("NAND install script sometime fails")
En relançant le script une seconde fois, cela semble bien se passer (les prérequis étaient une NAND formattée, c'est peut-être ce qu'a fait la première passe ...
On peut donc booter sans carte microSD, ce qui permettra de l'utiliser pour autre chose !
```bash
root@pcduino3nano:~# df -h

Filesystem  Size  Used Avail Use% Mounted on
/dev/root   3.6G  1.1G  2.4G  31% /
devtmpfs437M 0  437M   0% /dev
tmpfs   437M 0  437M   0% /dev/shm
tmpfs   437M   12M  426M   3% /run
tmpfs   5.0M  4.0K  5.0M   1% /run/lock
tmpfs   437M 0  437M   0% /sys/fs/cgroup
tmpfs   256M 0  256M   0% /tmp
/dev/nand1   32M  5.9M   27M  19% /boot

root@pcduino3nano:~# more /etc/fstab
```
# UNCONFIGURED FSTAB FOR BASE SYSTEM
```bash
tmpfs   /tmp tmpfs   nodev,nosuid,size=256M  0  0
/dev/nand2  /   ext4defaults,noatime,nodiratime,data=writeback,commi
t=600,errors=remount-ro0   0
/var/swap none swap sw 0 0
/dev/nand1 /boot vfatdefaults 0 0
```
# Pour avoir une interface graphique
(Branchement sur TV, ...)

Installer quelques paquets en plus, selon vos choix (incluant le serveur X, un gestionnaire de connexion, un gestionnaire de fenêtre :
```bash
apt install xserver-xorg lightdm xfce4
```
