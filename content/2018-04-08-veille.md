Title: Compilation des liens de la semaine 2018-04-08
Date:  2018-04-08
Tags:  veille, spark, kafka, test, angular, jwt, mesos, dcos
Category:  veille
Compilation des [liens](http://stream.logpickr.com/profile/daniel?_t=rss) de la semaine



# [Spark](http://stream.logpickr.com/profile/daniel?_t=rss/tag/spark)
REX - Industrialisation de jobs Spark dans le cadre d'un datalake
[http://blog.ippon.fr/2018/03/22/rex-industrialisation-jobs-spark-datalake](http://blog.ippon.fr/2018/03/22/rex-industrialisation-jobs-spark-datalake)









# Autres
Apache Kafka
[https://kafka.apache.org/11/documentation/streams/developer-guide/testing.html](https://kafka.apache.org/11/documentation/streams/developer-guide/testing.html)  _[#kafka](http://stream.logpickr.com/profile/daniel?_t=rss/tag/kafka)_    _[#test](http://stream.logpickr.com/profile/daniel?_t=rss/tag/test)_  

Angular Security - Authentication With JWT: The Complete Guide
[https://blog.angular-university.io/angular-jwt-authentication](https://blog.angular-university.io/angular-jwt-authentication)  _[#angular](http://stream.logpickr.com/profile/daniel?_t=rss/tag/angular)_    _[#jwt](http://stream.logpickr.com/profile/daniel?_t=rss/tag/jwt)_  

Apache Mesos and Kafka Streams for Highly Scalable Microservices
[https://www.confluent.io/blog/apache-mesos-apache-kafka-kafka-streams-highly-scalable-microservices](https://www.confluent.io/blog/apache-mesos-apache-kafka-kafka-streams-highly-scalable-microservices)  _[#kafka](http://stream.logpickr.com/profile/daniel?_t=rss/tag/kafka)_    _[#mesos](http://stream.logpickr.com/profile/daniel?_t=rss/tag/mesos)_    _[#dcos](http://stream.logpickr.com/profile/daniel?_t=rss/tag/dcos)_  





----
_[Source d'inspiration](https://blog.jbfavre.org/2016/08/26/compilation-veille-twitter-rss/)_