Title: Compilation des liens de la semaine 2017-04-09
Date:  2017-04-09
Tags:  veille, js, scala, mesos, dcos, stream, smack, flink, graph, angular, spark, test
Category:  veille
Compilation des [liens](http://stream.logpickr.com/profile/daniel?_t=rss) de la semaine



# [Spark](http://stream.logpickr.com/profile/daniel?_t=rss/tag/spark)
Running Spark Tests in Standalone Cluster - Eugene Zhulenev  #test
[http://eugenezhulenev.com/blog/2014/10/18/run-tests-in-standalone-spark-cluster](http://eugenezhulenev.com/blog/2014/10/18/run-tests-in-standalone-spark-cluster)  _[#test](http://stream.logpickr.com/profile/daniel?_t=rss/tag/test)_  

Making Apache Spark Testing Easy with Spark Testing Base - Cloudera Engineering Blog
[http://blog.cloudera.com/blog/2015/09/making-apache-spark-testing-easy-with-spark-testing-base](http://blog.cloudera.com/blog/2015/09/making-apache-spark-testing-easy-with-spark-testing-base)  _[#test](http://stream.logpickr.com/profile/daniel?_t=rss/tag/test)_  

Apache Spark - create and test jobs - SequenceIQ Blog
[http://blog.sequenceiq.com/blog/2014/09/29/spark-correlation-and-testing](http://blog.sequenceiq.com/blog/2014/09/29/spark-correlation-and-testing)  _[#test](http://stream.logpickr.com/profile/daniel?_t=rss/tag/test)_  


# [Streaming et Fast Data](http://stream.logpickr.com/profile/daniel?_t=rss/tag/stream)
How To Get Monitoring Right For Streaming And Fast Data Systems Built With Spark, Mesos, Akka, Cassandra and Kafka | @lightbend
[http://www.lightbend.com/blog/how-to-get-monitoring-right-for-streaming-and-fast-data-systems-built-with-spark-mesos-akka-cassandra-and-kafka](http://www.lightbend.com/blog/how-to-get-monitoring-right-for-streaming-and-fast-data-systems-built-with-spark-mesos-akka-cassandra-and-kafka)  _[#smack](http://stream.logpickr.com/profile/daniel?_t=rss/tag/smack)_  

From Streams to Tables and Back Again: An Update on Apache Flink’s Table & SQL API · data Artisans
[https://data-artisans.com/flink-table-sql-api-update](https://data-artisans.com/flink-table-sql-api-update)  _[#flink](http://stream.logpickr.com/profile/daniel?_t=rss/tag/flink)_  

Flink Aims to Simplify Stream Processing · Datanami
[https://www.datanami.com/2017/04/03/flink-aims-simplify-stream-processing](https://www.datanami.com/2017/04/03/flink-aims-simplify-stream-processing)  _[#flink](http://stream.logpickr.com/profile/daniel?_t=rss/tag/flink)_  







# Autres
More Than React, Part 2: How Binding.scala Makes Reusability Easy
[https://www.infoq.com/articles/more-than-react-part-2-reusability](https://www.infoq.com/articles/more-than-react-part-2-reusability)  _[#js](http://stream.logpickr.com/profile/daniel?_t=rss/tag/js)_    _[#scala](http://stream.logpickr.com/profile/daniel?_t=rss/tag/scala)_  

Introducing Marathon 1.4 · Mesosphere
[https://mesosphere.com/blog/2017/03/29/introducing-marathon-1-4](https://mesosphere.com/blog/2017/03/29/introducing-marathon-1-4)  _[#mesos](http://stream.logpickr.com/profile/daniel?_t=rss/tag/mesos)_    _[#dcos](http://stream.logpickr.com/profile/daniel?_t=rss/tag/dcos)_  

LDBC Developer Community: Benchmarking Graph Data Management Systems - ODBMS.org
[http://www.odbms.org/2017/04/ldbc-developer-community-benchmarking-graph-data-management-systems](http://www.odbms.org/2017/04/ldbc-developer-community-benchmarking-graph-data-management-systems)  _[#graph](http://stream.logpickr.com/profile/daniel?_t=rss/tag/graph)_  

Long Term Support for Angular Announced at ng-conf 2017
[https://www.infoq.com/news/2017/04/ng-conf-2017-keynote](https://www.infoq.com/news/2017/04/ng-conf-2017-keynote)  _[#angular](http://stream.logpickr.com/profile/daniel?_t=rss/tag/angular)_  





----
_[Source d'inspiration](https://blog.jbfavre.org/2016/08/26/compilation-veille-twitter-rss/)_