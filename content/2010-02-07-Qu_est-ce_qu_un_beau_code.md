Title: "Qu'est-ce qu'un beau code ?"
modifier: "Daniel"
created: "201002071309"
tags: computing, placedelatoile, todo, grec, franceculture, radio



Je deviens fan de l'émission de France Culture [Place de la toile](http://sites.radiofrance.fr/chaines/france-culture2/emissions/place_toile/?diffusion_id=80736&tp=lire&pg=index)

Je  liste ici quelques réflexions personnelles suite à  l'écoute de l'émission, et peut être donner susciter quelques autres pistes.

Merci aux intervenants de la première partie qui ont ouvert des perspectives intéressantes sur l'utilisation un peu décalée du code informatique (poésie, obfuscation, concours de style, ...)

Je crois toutefois qu'ils n'ont pas entièrement répondu à votre excellente question initiale 'qu'est-ce que du beau code ?' 

J'en tiens pour preuve leur réponse à votre sous question 'est-ce simplement du code qui fonctionne ?'

Je crois que les programmeurs qui qualifient du code 'beau' ou 'pas beau' font en général référence dans les deux cas à du code qui fonctionne, mais dont la différence tient à la beauté, la concision, la lisibilité (propriété qui fait qu'un autre programmeur quel que soit son niveau peut rapidement comprendre l'intention du code pour le faire évoluer). Le lien avec l'élégance d'une demonstration mathématique est tout à fait pertinent.

Il n'est pas sans rapport avec les notions de 'design pattern' qui tentent de formaliser et de décrire, indépendamment du langage utilisé, des schémas d'implémentation ou d'architecture du code qui favorisent sa qualité, sa réutilisabilité, sa lecture et sa compréhension. A ce sujet, ' Patterns of Software Tales from the Software Community' de   Richard P. Gabriel     OXFORD UNIVERSITY PRESS,    1996 établit le lien avec les travaux de Christopher Alexander, qui lui est à l'origine un architecte constructeur de bâtiments

Pour continuer sur votre étonnement initial lié à une expression utilisée par des programmeurs, deux autres idées:

la notion de 'code mort' qui désigne une portion de code qui ne sera jamais irriguée par le flux d'éxécution d'un programme, quel que soit l'interaction que l'on peut avoir avec lui. Il s'agit en général de code que l'on peut supprimler sans changer le fonctionnement du programme.

Il existe toute une série de programmes chargés d'analyser le code source, pour y déceler de potentielles erreurs, recommander de bonnes pratiques, automatiquement 'comprendre' l'intention originelle et suggérer des pistes d'amélioration. En somme rendre le code plus 'beau' (ou efficace ?)

Enfin, plus ludique, l'utilisation des commentaires (partie libre du code, sans impact sur l'éxécution, ou le programmeur a la possibilité de décrire en termes humains l'intention du code informatique). Il est souvent le lieu d'écarts poétiques, de billets d'humeurs, comme les annotations que certains moines copistes pouvaient laisser dans la marge  des parchemins du moyen âge.

L'étude de l'histoire des langages informatiques montre des parentées entre les langages, avec les inévitables querelles entre les partisans de tel ou tel langage, qui alimentent parfois de véritables affrontements sur la supériorité de tel ou tel langage. On peut mettre en parallèle avec la querelle Mac vs Pc vs linux (vs Google bientôt ?), et si j'osais, les querelles entre anciens et modernes, ou celles qui virent s'affronter les mouvements d'idée au sein de la Sorbonne, ou des églises lors des guerres de religion.

Je constate régulièrement  la difficulté qu'ont certains programmeurs 'élevés' dans un langage de programmation d'un certain type à utiliser un langage autre, tout comme l'on éprouve des difficultés à apprendre une langue étrangère.

La remarque de Clarisse Herrenschmidt en fin d'émission sur l'apprentissage à l'école est tout à fait intéressante, à l'heure ou l'apprentissage se résume trop souvent à une simple utilisation d'un logiciel, voir pire à la simple maitrise d'un logiciel X.

Son utilisation (à dessein ?) du mot de réflexivité incomplète me renvoie irrésistiblement à l'énoncé du théorème  d'incomplétude de Gödel, et par ricochet au magnifique livre de Douglas Hofstater 'Gödel, Escher, Bach: An Eternal Golden Braid '.

Je suis d'accord avec elle pour dire que la complétude des langages informatique n'est pas totale, toutefois la réfléxivité et l'autodescription des langages informatiques sont à la base de nombreux travaux théoriques et pratiques dans ce domaine.

La récente découverte du reportage de Robert Cringuely 'Triumph of the Nerds'  (vf : les cinglés de l'informatique) m'a fait redécouvrir le lien entre les travaux du Palo Alto Research Center (dont Adele Goldberg, référence pour ceux qui se sont un peu intéressés aux langages à Objets), et certains sources d'inspiration de Steve Jobs (je crois qu'il utilise lui même le mot de 'révélation')

Dans le même reportage, Jobs évoque je crois sa différence d'approche par rapport à Microsoft en terme de culture, de beauté.

Alors, qui sera le prochain sémiologue de l'informatique ?

Voilà, j'attends avec intérêt ce que peut donner la suggestion de benjamin sur l'application de l'école de pensée de Jacques Derrida au domaine de l'informatique ou de l'internet

PS : merci aussi pour votre sentiment sur l'émission d'Arte de mardi prochain, à l'heure ou l'on justifie des lois sur le filtrage de l'internet par la présence de pédopornographes sur les réseaux. La recherche récente  de Fabrice Epelboin sur le sujet est particulièrement intéressant
