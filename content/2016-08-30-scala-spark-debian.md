Title:  Installation d'une version récente de scala et spark sur debian
Tags:  scala, spark, debian
Category: bigdata

Même si scala est présent dans les référentiels debian, il peut être nécessaire d'installer une version plus récente (en phase avec une version récente de spark).

Les pages de téléchargement fournissent des binaires à décompresser ([Scala](http://www.scala-lang.org/download/), [Spark](http://spark.apache.org/downloads.html)), reste à configurer les variables d'environnement pour que tout soit accessible proprement.

* configuration Scala : [Getting started](http://www.scala-lang.org/documentation/getting-started.html)

```bash
    Environment 	Variable 	Value (example)
    Unix 	$SCALA_HOME 	/usr/local/share/scala
    $PATH 	$PATH:$SCALA_HOME/bin
    Windows 	%SCALA_HOME% 	c:\Progra~1\Scala
    %PATH% 	%PATH%;%SCALA_HOME%\bin
```

Le [Wiki debian](https://wiki.debian.org/EnvironmentVariables) nous explique comment procéder

> Bash first reads /etc/profile to get values that are defined for all users. After reading that file, it looks for ~/.bash_profile', ~/.bash_login', and `~/.profile', in that order, and reads and executes commands from the first of these files that exists and is readable

> Insert all personal definitions into ~/.profile

```bash
SCALA_HOME=/opt/scala-2.11.8
PATH=$PATH:$SCALA_HOME/bin
SPARK_HOME=/opt/spark-2.0.0-bin-hadoop2.7
PATH=$PATH:$SPARK_HOME/bin
```
> Create or edit file ~/.bash_profile and include commands:

```bash
if [ -f ~/.profile ]; then
   . ~/.profile
fi
```
