Title: Lancement d'une commande bi-hebdomadaire dans crontab
Date:  2018-04-15
Tags:  cron, crontab, howto
Category:  linux

# Déclencher une commande toutes les deux semaines

La syntaxe de base de `cron` ne semble pas permettre ce genre de manipulation, mais en couplant avec une commande initiale,  on peut s'assurer qu'on l'exécute exactement à la période souhaitée.

## Exemple

```
0 20 8-14,22-28 * * test $(date +\%u) -eq 7 && myscript.sh
```

## Commentaire 

la séquence `8-14,22-28` permet de s'assurer que l'on n'effectuera la commande que entre le 8 et le 14 du mois, puis entre le 22 et le 28 du mois.

A ce stade, la commande est exécutée 14 fois dans le mois, aux dates citées. Pour restreindre l'exécution aux dimanches situés dans ces périodes, on préfixera la commande finale par un test sur le jour de la semaine (ici 7 = dimanche). La plage 8-14 permet de s'assurer que l'on tombera sur un dimanche durant n'importe quel mois de l'année.

```
test $(date +\%u) -eq 7
```

Si ce test fonctionne (le jour d'exécution est un dimanche), le reste de la ligne après && sera exécuté, sinon on s'arrêtera après l'échec du test.


```
0 20 8-14,22-28 * * test $(date +\%u) -eq 7 && myscript.sh
```