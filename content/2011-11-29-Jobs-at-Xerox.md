tags: Apple, Smalltalk, Goldberg, Xerox, Jobs, Citations
title: Steve Jobs et Smalltalk  

On lit pas mal d’
[articles](http://www.kelblog.com/article-lisez-la-bio-de-steve-jobs-90594655.html)
et de bouquins sur la vie et l’oeuvre de Steve Jobs, j’aimerais apporter
une petite pierre à l’édifice en pointant un épisode qui me semble
fondateur :  
La mythique démo de Smalltalk par [Adele
Goldberg](http://en.wikipedia.org/wiki/Adele_Goldberg_%28computer_scientist%29)
(Xerox Parc, chef de projet de l’environnement Smalltalk chez Xerox) à
Steve Jobs et son équipe.

On peut en retrouver une trace dans le film de Bob Cringely ‘The Triumph
of the Nerds’ (Les Cinglés de l’informatique) dont on peut trouver une
retranscription ici :

<http://www.pbs.org/nerds/part3.html>

Steve Jobs est interviewé dans le film et déclare :

“And they showed me really three things. But I was so blinded by the
first one I didn’t even really see the other two.
  
 One of the things they showed me was object orienting programming they
showed me that but I didn’t even see that.
  
 The other one they showed me was a networked computer system…they had
over a hundred Alto computers all networked using email etc., etc., I
didn’t even see that.
  
 I was so blinded by the first thing they showed me which was the
graphical user interface.
  
 **I thought it was the best thing I’d ever seen in my life.**
  
 Now remember it was very flawed, what we saw was incomplete, they’d
done a bunch of things wrong.
  
 But we didn’t know that at the time but still though they had the germ
of the idea was there and they’d done it very well and within you know
ten minutes it was obvious to me that all computers would work like this
some day.”

It was a turning-point. Jobs decided that this was the way forward for
Apple.

