Title: Ubuntu au boulot
tags: boulot, linux, ubuntu


Depuis environ un mois, j'utilise Ubuntu 14.10 **au boulot** comme système d'exploitation principal.

Avec en plus une machine virtuelle Windows pour accéder à un VPN, éditer sous Word, Excel.

* comment installer [VMWare](https://help.ubuntu.com/community/VMware/Player) 

Sinon, tout le reste est géré à partir de linux :

* mail (thunderbird) + calendrier en lecture (extension ligthning)
* accès disques réseau (samba)
* imprimante réseau (samba)
* messagerie instantanée (pigdin)
* ...
* geany remplace avantageusement notepad++
* rabbitvcs suit de près les fonctionnalités de tortoisesvn (utiliser les paquets de [rabbitvcs](http://wiki.rabbitvcs.org/wiki/install/ubuntu))

Ma machine a retrouvé du répondant, j'ai retrouvé de la sérénité et de l'organisation (pas que par ce biais, hein ...)

