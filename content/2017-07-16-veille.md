Title: Compilation des liens de la semaine 2017-07-16
Date:  2017-07-16
Tags:  veille, druid, spark, stream, kafka
Category:  veille
Compilation des [liens](http://stream.logpickr.com/profile/daniel?_t=rss) de la semaine



# [Spark](http://stream.logpickr.com/profile/daniel?_t=rss/tag/spark)
Introducing Apache Spark 2.2 - The Databricks Blog
[https://databricks.com/blog/2017/07/11/introducing-apache-spark-2-2.html](https://databricks.com/blog/2017/07/11/introducing-apache-spark-2-2.html)


# [Streaming et Fast Data](http://stream.logpickr.com/profile/daniel?_t=rss/tag/stream)
Exactly-once Semantics are Possible: Here’s How Kafka Does it · Confluent
[https://www.confluent.io/blog/exactly-once-semantics-are-possible-heres-how-apache-kafka-does-it](https://www.confluent.io/blog/exactly-once-semantics-are-possible-heres-how-apache-kafka-does-it)  _[#kafka](http://stream.logpickr.com/profile/daniel?_t=rss/tag/kafka)_  







# Autres
Druid: Sub-Second OLAP queries over Petabytes of Streaming Data
[https://www.slideshare.net/Hadoop_Summit/druid-subsecond-olap-queries-over-petabytes-of-streaming-data](https://www.slideshare.net/Hadoop_Summit/druid-subsecond-olap-queries-over-petabytes-of-streaming-data)  _[#druid](http://stream.logpickr.com/profile/daniel?_t=rss/tag/druid)_  





----
_[Source d'inspiration](https://blog.jbfavre.org/2016/08/26/compilation-veille-twitter-rss/)_