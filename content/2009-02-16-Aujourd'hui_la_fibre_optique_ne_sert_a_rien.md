Title: Aujourd'hui, la fibre optique ne sert à rien ...
tags: net


Dans la tribune du 14 février (revue de presse FT)
Philippe Capron, directeur financier de Vivendi
"Aucun revenu, aucun service supplémentaire à mettre en face d'investissements considérables. Cela peut seulement encourager à un peu plus le téléchargement illégal de films"

Que c'est beau d'être visionnaire !
A garder dans les annales, c'est l'illustration parfaite du conflit entre l'économie traditionnelle et la "perturbation" qu'apporte internet.

Attendons la réaction de [JM Billaut](http://billaut.typepad.com/) sur le sujet ...

màj 17/02/2009 : Ca n'a pas trop tardé :

http://www.glazman.org/weblog/dotclear/index.php?post/2009/02/17/Comme-d-hab

http://www.degroupnews.com/actualite/n3308-fibre_optique-deploiement-ftth-vivendi-sfr.html?xtor=RSS-1

Mais que fait JM Billaut ?! (il doit être en voyage d'étude ...)

http://www.jmp.net/index.php/internet/dangers/224-luc-besson-les-operateurs-et-google-sont-dans-un-bateau-#mce_temp_url#
