published: true
date: 2012-05-14
tags:  Nokia, n9, gpodder , ProfileMatic, en , meego
Title: Automating podcast download with gPodder on a Nokia N9

I'm a proud and happy owner of a Nokia N9 smartphone since a few months, and [gPodder](http://gpodder.org) was one of the first application I wanted to install, in order to manage my podcasts.

The interface is slightly different from the desktop version I use on various machines (mostly linux), but the command line interface is still present.

As you may know (or not), it is possible to invoke gpodder from the command line to update or download your configured podcasts list

```bash
gpo update; gpo download
```
Provided you have a current internet connection (gPodder doesn't try to open one : bug or feature ?), it updates your episodes list and downloads them for an offline replay, without launching the user interface (which is quite long to launch by the way).

I hoped I could find crontab or an equivalent on meego harmattan, but it [seems it's not the case](https://joindiaspora.com/posts/1557245)

I found a workaround after installing [ProfileMatic]:(http://store.ovi.com/content/216122) (a tool to automatically manage profiles and actions according to time or other input conditions. Among the possible actions, one is to call a custom action through shell command. I was almot there !

The first try was to put directly the above shell command into the ProfileMatic custom command fields.

This didn't seem successful, and later found that the terminal could be invoked from there.
```bash
meego-terminal -n -e gpo update; gpo download
```
Even quoting the command after -e, this seem unsuccessful from within ProfileMatic, and the working solution was to separate the two subcommands in two different profiles launched atfer one minute (the minimum available) :
 
```bash
meego-terminal -n -e gpo update
meego-terminal -n -e gpo download
``` 
There is possibly a better solution, ( suggestions welcome ), but this one is currently working for me.
 
Side Note : in order to have an automatic established Wi-Fi connection ( other suggestions still welcome), I switched "Fligh mode" to off and "Battery saving" to off in the actions of a previous task, one minute before. 
 
 
The  Wi-Fi connection is also declared as "automatic" in my N9 configuration.
