Title: Transposition de données avec Talend
tags: talend, etl
category: bigdata

On trouve régulièrement dans les bases de données des enregistrements de structure :
```sql
ID ; CLE ; VALEUR 
```
Exemple de jeu de données :
```sql
ID1;ATTRIBUT1;VALEUR11
ID1;ATRRIBUT2;VALEUR21
ID2;ATTRIBUT1;VALEUR12
ID3;ATTRIBUT1;VALEUR31
ID3;ATRRIBUT2;VALEUR32
```
On souhaiterait obtenir une sorte de matrice transposée, pour un ID donné , la structure suivante :
```sql
ID; ATTRIBUT1; ATTRIBUT2
```
Exemple pour le même jeu de données :
```sql
ID1;VALEUR11;VALEUR12
ID2;VALEUR21;
ID3;VALEUR31;VALEUR32
```
Le composant Talend [tPivotToColumsDelimited](https://help.talend.com/display/TalendOpenStudioforBigDataComponentsReferenceGuide520EN/13.41+tPivotToColumnsDelimited) permet exactement ce genre de transformation.

Curieusement, il force toutefois à récupérer la sortie dans un fichier plat, alors que la plupart des autres composants de trnasformation donnent un flux en sortie (que l'on peut stocker sur fichier, en base, ou envoyer dans un autre flux).


[Lien](http://www.developpez.net/forums/d1314039/logiciels/solutions-d-entreprise/business-intelligence/talend/developpement-jobs/transposer-forme-tabulaire-table-sgbd-vers-forme-matricielle-excel/)

[Lien2](http://dwetl.com/2015/01/09/split-rows-to-columns/)
