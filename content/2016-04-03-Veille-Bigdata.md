Title: Veille Big Data
category: bigdata
tags: rss, veille,journalduhacker

Vous pouvez suivre aux liens suivants ma veille sur le sujet Big Data :

 [Veille Big Data](https://tintouli.withknown.com/tag/bigdata)
 
[Veille Big Data (flux RSS)](https://tintouli.withknown.com/tag/bigdata?_t=rss)

Autre source/contributions sur l'excellent [Journal du hacker](https://www.journalduhacker.net)

[Marque 'mégadonnées' sur le Jdh](https://www.journalduhacker.net/t/m%C3%A9gadonn%C3%A9es)
