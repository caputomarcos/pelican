Status: draft
Title: "Public consultation on specific aspects of transparency, traffic management and switching in an Open Internet"
modifier: "Daniel"
tags:  europe, netneutrality, en 
  
Below, my answers to the "Public consultation on specific aspects of transparency, traffic management and switching in an Open Internet":http://ec.europa.eu/yourvoice/ipm/forms/dispatch?form=NetNeutralityIndiv

It was long, sometimes boring and puzzling, but I made it !

(tip : at the end of the consultation, you can save your answers as pdf, and then linux users can use pdf2text for example)

----------

Public consultation on specific aspects of transparency, traffic management and switching in an Open Internet

Questionnaire

General information
Question 1: a) Please provide a brief description of your interest in open Internet issues.

-open reply-(compulsory)

Individual engineer interested in net neutrality 

b) Please provide your name, postal and e-mail address and if you wish, your telephone number for any questions on your contribution. -open reply-(compulsory)

(HIDDEN)

c) In which Member State(s) do you live?

-open reply-(compulsory)

France 

Does your answer to this question (a,b or c) contain confidential information? -single choice

reply-(compulsory)
Yes
 
Please provide a non-confidential answer to this question. -open reply-(compulsory)
cf a) 
1. Traffic management
1.1. Traffic management and differentiation
Question 2: Please provide your views on the following ways/situations where traffic management may be applied by ISPs.   Are traffic management measures: a) applied to deliver managed services (e.g. to ensure a guaranteed quality of service for a specific content/applications)
-single choice reply-(optional)
problematic
 
Please explain your response
-open reply-(compulsory)
1/ ISP primarily provide infrastructure, and connect to a global network called internet. Global QoS for services is managed on the internet by global technical norms established by IETF or other normative organisms, not by individual ISPs. The risk is to favoorize one's content/application to the detriment of other concurrents 2/ QoS is ensured by sufficient bandwidth, or other techniques, not exclusively by traffic management 
b) taking into account the sensitivity of the service to delay or packet loss
-single choice reply-(optional)
problematic
 
Please explain your response
-open reply-(compulsory)
This issue is not managed by traffic management exclusively but redundancy and other techniques 
c) used to implement or manage compliance with the explicit contractual restrictions (e.g. on P2P or VoIP) of the Internet access product accepted by the user -single choice reply-(optional)
Please explain your response
-open reply-(compulsory)
problematic
 
same as a) VoIP does not require enormous bandwith, in comparison to HD video 
d) targeting types/classes of traffic contributing most to congestion
-single choice reply-(optional)
problematic
 
Please explain your response
-open reply-(compulsory)
ISPs used to manage dedicated networks for specific content (video, tv, vod), which were far more expensive than the internet. They all use direct internet now. Congestion issues can also be adressed by increasing the global bandwidth (fiber network, ...) or using other wireless technologies. Using traffic management to address congestion problem is a paradox : extra impact on the network , for short-sight problem solving, instead of investing in better technical solutions 
e) targeting heavy users whose use is excessive to the extent that it impacts on other users
-single choice reply-(optional)
problematic
 
Please explain your response
-open reply-(compulsory)
same as d) 
f) applied during busy times and places, when and where congestion occurs
-single choice reply-(optional)
problematic
 
Please explain your response
-open reply-(compulsory)
same as d) 
g) affecting all applications/content providers in the same way (application-agnostic)
-single choice reply-(optional)
appropriate
 
Please explain your response
-open reply-(compulsory)
only if global and standardized norm applied to all providers 
h) affecting (similar) applications/content
-single choice reply-(optional)
appropriate
providers of the same category in the same way  
Please explain your response
-open reply-(compulsory)
same as g) 
i) used, without other grounds, against services
problematic
competing with the ISP's own services
-single choice reply-(optional)
 
Please explain your response
-open reply-(compulsory)
as in other domains, does not go in the trend of separating infrastructure and service providers (e.g railways, energy, ...) 
j) implemented at the full discretion of the ISP
-single choice reply-(optional)
problematic
 
Please explain your response
-open reply-(compulsory)
same as previous answers ... 
k) other differentiation criteria (please specify)
-open reply-(optional)
 
Please explain your response.
-open reply-(compulsory)
in general traffic management does not apper to be a proper (and unique) solution to congestion and QoS, and raises the issue of free concurrence between actors 
Does your answer to this question (a, b, c, d, e, No f, g, h, i, j or k) contain confidential information?  
-single choice reply-(compulsory)
1.2 Traffic management and privacy issues
Question 3:  Where the user's consent is required for traffic management measures, particularly where such measures might entail access to and analysis of certain personal data by ISPs, please explain how (e.g. in which format) this consent should be sought by the ISP, what prior information needs to be provided by the ISP to the user, and how the user consent should be given, in order to optimise user awareness and user convenience.
-open reply-(optional)
In no case this should be allowed to ISPs. generally the average user is not able to evaluate the impact of such measures on its privacy (and QoS either) see below : >The BEREC investigation has revealed that many consumers have Internet access subscriptions with a number of restrictions This is a obvious proof of the above statement 
Does your answer to this question contain confidential information? -single choice reply(compulsory)
No
 
2. Transparency and switching (consumer choice)
2.1 Transparency and general characteristics of the Internet access offer
Question 4:  In order to allow consumers to make informed choices, on the basis of clear, meaningful, and comparable information, which elements should be communicated to consumers? - Elements related to traffic management practices: a) Contractual restrictions (blocking, throttling, other
important
 
restrictions on application use)
-single choice reply-(optional)
Please provide reasons for your answer:
-open reply-(optional)
Even if technically difficult to understand, it is a criteria of comparison, as well as bandwidth or used technology. A problem is that the offer may be as difficult to read as an insurance policy ... 
b) Traffic management policy applied to prioritise certain traffic in specific circumstances
-single choice reply-(optional)
important
 
Please provide reasons for your answer:
-open reply-(optional)
same as a) 
c) Whether and to what extent managed services may affect the quality of the best effort Internet (e.g. the possibility of the Internet connection being affected when watching IP-TV or when using other managed services)
-multiple choices reply-(optional)
important
 
Please provide reasons for your answer:
-open reply-(optional)
same as a) 
d) Other restrictions, please specify:
-open reply-(optional)
specific protocols blocked, bandwidth limitation in some conditions, extra billing, ... global firewalling or natting for the whole network (such as so called mobile internet access) 
e) Data allowances (caps), download limits
-single choice reply-(optional)
important
 
Please provide reasons for your answer:
-open reply-(optional)
same as a) 
f) What these data allowances enable customers to do in practice (download x hours of video; upload y photos etc.)
-single choice reply-(optional)
important
 
Please provide reasons for your answer:
-open reply-(optional)
 
Elements related to speed and quality:
important
a) Average speed, typical speed ranges and speed at   peak times (upload and download)
-multiple choices reply-(optional)
Please provide reasons for your answer:
-open reply-(optional)
same as a) 
b) Respect of guaranteed minimum speed (if applicable)
-multiple choices reply-(optional)
important
 
Please provide reasons for your answer:
-open reply-(optional)
same as a) 
c) What these speeds allow customers to do in practice (video-streaming, audio-download, video-conferences etc.)
-single choice reply-(optional)
important
 
Please provide reasons for your answer:
-open reply-(optional)
same as a) 
d) Latency/network responsiveness (a measure of traffic delay) and which services would be affected thereby (e.g. certain applications such as IP-TV or videoconferencing would be more seriously impacted by higher traffic delays in the network of the provider)
-multiple choices reply-(optional)
important
 
Please provide reasons for your answer:
-open reply-(optional)
same as a) 
e) Jitter (a measure of the variability over time of latency) and which services would be affected thereby (e.g. echoing in VoIP calls)
-multiple choices reply-(optional)
important
 
Please provide reasons for your answer:
-open reply-(optional)
same as a) 
f) Packet loss rate (share of packets lost in the network) and which services would be affected thereby (e.g. VoIP)
-multiple choices reply-(optional)
important
 
Please provide reasons for your answer:
-open reply-(optional)
same as a) 
g) Reliability of the service (network accessibility and retainability), i.e. measure for successful start and completion of data sessions
-multiple choices reply-(optional)
important
 
Please provide reasons for your answer:
-open reply-(optional)
same as a) 
h) Quality parameters for (mobile) voice telephony (call setup success rate, dropped calls, speech
important
 
quality, other)
-multiple choices reply-(optional)
Please provide reasons for your answer:
-open reply-(optional)
same as a) 
i) Other, please specify:
-open reply-(optional)
 
Does your answer to question 12 (or to any of its sub-questions) contain confidential information? -single choice reply-(compulsory)
Question 5: Some ISPs currently apply 'fair use policies', which give them wide discretion to apply restrictions on traffic generated by users whose usage they consider excessive. Do you consider that, in case of contractual restrictions of data consumption, quantified data allowances (e.g. monthly caps of x MB or GB) are more transparent for consumers than discretionary fair use clauses?
-single choice reply-(optional)
No
 
No
 
Please provide reasons for your answer.
-open reply-(compulsory)
No, because usually, fair clauses are themselves capped by a maximum, which can be easily understood by average customers. The multiplication of capped offer would increase complexity of comparison between offers (cf previous question answer) 
Does your answer to this question contain confidential information? -single choice reply(compulsory) Question 6:  a) When should the elements of information referred to in question 4 be provided to the consumer by the ISP?
-multiple choices reply-(optional)
No
 
before signing the contract - during the contract period if changes occur - other
 
Please specify: -open reply-(compulsory)
But those clauses should not be multiplied and unreadable, and severely monitored by EU institutions 
b) Which format (e.g. contract, general terms and conditions, separate and specific information, other (please specify)) do you consider appropriate to communicate this information to consumers?
-open reply-(optional)
separate and specific information, but short, concise and comparable 
Does your answer to this question contain confidential information? -single choice reply(compulsory) Question 7: a) In order to promote transparency and consumer choice, do you consider it necessary that comparable data on the Internet access provided by ISPs is
No
 
Yes
 
collected and published by NRAs or another independent organisation?  
-single choice reply-(optional)
Please explain your response.
-open reply-(compulsory)
Its even a prerequisite before ISPs may be allowed to do such limitations (and as few as necessary, the general rule being no cryptic clauses) 
Do you think this information should be broken down by geographic areas or different data plans? -open reply-(optional)
no, never 
b) What are the advantages and corresponding costs of this data collection and publication being undertaken by NRAs or by another type of organisation (please specify which one). Please provide an estimate at EU-level or for an EU Member State of your choice.
-open reply-(optional)
advantage : consumer transparency cost: supported by ISPs, when allowed to implement restrictions and traffic limitation 
Does your answer to this question (a or b) contain confidential information? -single choice
reply-(compulsory)
No
 
Question 8: a) Do you consider it necessary to regulate the labelling as "Internet access" of subscriptions that restrict access to some Internet services, content or applications?
-single choice reply-(optional)
Yes
 
Please reason your answer. -open reply-(compulsory)
It's just a question of equity and transparency 
b) If yes, which restrictions would be acceptable before a subscription could no longer be marketed, without qualification, as an “Internet access” product”?
-open reply-(optional)
None, why such question ? Generally, mobile internet acces not considered as regular internet access because no specific IP for end users, some protocols are blocked, etc ... 
c) What would be the consequences (including the cost) for ISPs if they were not allowed to market as ‘Internet access’ an offer with certain restrictions, or if such marketing was subject to mandatory qualification? Please provide quantification for your own company or an ISP of your choice explaining your assumptions and methodology.
-open reply-(optional)
Obviously they would be mandated to improve their offer, or clearly explicit their constraints. 
Does your answer to this question (a, b or c) contain confidential information? -single choice
reply-(compulsory)
No
 
2.2 Switching
Question 9: a) Please explain what barriers to switching ISPs still exist (if any) and how they can be overcome. Please mention in your reply all direct and indirect factors dissuading consumers from switching (e.g. obstacles linked to the terminal equipment, burden of proof regarding a possible breach of contract, etc.)
-open reply-(optional)
- subventioned equipment and so called 'fidelity points' (should be banned, any device should be compliant with any offer, besides harware or software compatibility) - switch period without access (an alternate access could be offered meanwhile, e.g. 3G connection
while ADSL switching) - ISP provided identity or data retrieval (such as mail address, which is a clear confusion between infrastructure and service, or photo not accessible after a switch because not easily retrievable , or dependant of ISP single sign on to be accessed) 
b) How should an ISP inform consumers of changes to their packages?
-open reply-(optional)
by postal address, additionaly to any other electronic information 
c) What actions by an ISP would constitute a breach of contract or modifications to the contractual conditions which would enable a consumer to be released from a contract?
-open reply-(optional)
Any additional limitation not mentioned in the contract, and not caused by major justification. 
d)  Should customers be able to easily opt out from certain contractual restrictions (up to a completely unrestricted offer) by the same operator?
-single choice reply-(optional)
Yes
 
Please explain your response.
-open reply-(optional)
The contrary would be an incentive to switch to an other ISP. (but requires a free and concurrent market) 
If yes, how could this be facilitated?
-open reply-(optional)
restrictions should be as few as possible, clearly inventoried by authorities, and not subject to negociation between ISP and user. They must be mentioned in the contract and be as easily operated as possible by the end-user itself. 
e) Do you think that a customer should be allowed to
Yes
switch to another operator within a reduced contract   termination period in case his/her current operator does not at all offer an unrestricted Internet access product or does not allow switching to such unrestricted offer?
-single choice reply-(optional)
Please provide reasons for your response.
-open reply-(compulsory)
Obviously yes, why constraining a customer to use a restricted offer when he has a better choice ? May I have wrongly interpreted the question ? By the way, why allowing long contract termination periods ? 
Does your answer to this question (a, b, c, d or e) contain confidential information? -single choice
reply-(compulsory)
No
 
Question 10: While there may be valid (technical) reasons why consumers do not always get the advertised service speed or quality, should there be a limit on the discrepancy between advertised and actual service parameters (e.g. speed)?
-single choice reply-(optional)
Yes
 
Please explain your response. -open reply-(compulsory)
Yes, please, just a question of equity ... In other businesses, when a vendor does not provide the promised offer, he has to at least justify himself, at most compensate or be sued ... 
If you consider that there should be a limit on the discrepancy, how should this limit be defined? -open reply-(optional)
to be defined by local regulatory offices, depends on the discrepancy and the circumstances in which they occur. 
Does your answer to this question contain confidential information? -single choice reply(compulsory) Question 11:
No
 
Pursuant to Article 30 (6) of the Universal Service Directive conditions and procedures for contract termination shall not act as a disincentive against changing service providers. How could changing of operators be facilitated? Please provide examples and explain your response.
-open reply-(optional)
 
Does your answer to this question contain confidential information? -single choice reply(compulsory) Question 12:
No
 
How could the transparency of bundles (packages including telephony, Internet, TV) be improved for consumers and how could switching be facilitated in the presence of bundles?
-open reply-(optional)
bundles should be optional and the price not dependant of their combination (e.g addition of several parts should be easily calculated or detailed) However, some extra services do not cost extra money for ISPs, the main cost being infrastructure deployment 
Does your answer to this question contain confidential information? -single choice reply(compulsory) Question 13:
No
 
very important
a) How important would be the benefits for end-users   of improved transparency and facilitated switching?
-single choice reply-(optional)
Please explain your response.
-open reply-(compulsory)
It is a required condition of a free market without vendor lock-in. 
b) What would be the expected benefits in terms of innovation by new businesses (content or applications) as a consequence of improved consumer choice and increased competition between ISPs?
-open reply-(optional)
A very good incentive to propose something really different from the other concurrents (other than bandwidth or common services or limitation of resources).  
Does your answer to this question (a or b) contain confidential information? -single choice
reply-(compulsory)
No
 
3. Process
Question 14: a) Do you consider that intervention by public authorities is necessary at this stage?
-single choice reply-(optional)
Yes
 
If so, what would be the appropriate level of such intervention? -open reply-(optional)
Local independant authorities, possibly backed up by european ones. Based on international norms for technical aspects. 
b) What would be the consequences of divergent interventions by public authorities in the EU Member States?
-open reply-(optional)
Inefficient policy and non-incentive for ISPs who are mostly inter EU states or even worldwide companies 
Does your answer to this question (a or b) contain confidential information? -single choice
reply-(compulsory)
No
 
Question 15:
Yes
Under article 22(3) USD NRAs have the power to set   minimum quality of service requirements on undertakings providing public communications networks. In a scenario where in a given MemberStateno unrestricted offer is available (for instance because all operators actually block VoIP), do you consider that the "minimum quality of service tool" should be applied by the NRA to require operators to provide certain unrestricted offers?
-single choice reply-(optional)
Please explain your response.
-open reply-(compulsory)
Not sure to have all the information to understand the question, but no specific justification for blocking VoIP bys ISPs has been provided until now, other than preserving specific revenues to develop their infrastructure. (which is a statement I don't agree with). If it was a temporary constraint decided by regulatory authorities to help the emergence of a new operator, I would agree with it, but only for a limited and non-extensible period 
Does your answer to this question contain confidential information? -single choice reply(compulsory)
No
 

