Title: Flux RSS du Journal du hacker
tags: journalduhacker

* [Généralités](#generalites)
* [Flux Généraux](#generaux)
* [Flux des marques](#marques)
* [Oui mais encore ?](#ouimais) : c'est là que j'ai eu la révélation !

Le Journal du hacker fournit plein de [flux RSS](https://fr.wikipedia.org/wiki/RSS) pour vos différents usages.

Vous pouvez les découvrir en  utilisant Firefox par exemple, en cliquant sur l'icone des flux RSS (S'abonner)  depuis le menu graphique de droite (les trois barres horizontales)

![flux depuis Firefox](https://blog.journalduhacker.net/data/medias/flux.png)

## <a name="generalites"></a>Généralités ##

Mais avant d'en lister quelques uns,  quelques considérations :

*  Chaque page du Jdh a potentiellement son flux RSS
* Le contenu d'une page peut être modifié, en fonction de votre choix de filtre
* L'url et le contenu du flux peuvent être différents si vous êtes connecté ou pas, en fonction de votre choix de filtre

Les urls ci-dessous sont les urls généraux: lorsque vous êtes connecté,  ces mêmes flux sont personnalisables grâce aux filtres (voir plus bas).

## <a name="generaux"></a>Les flux généraux ##

* [Le flux principal](https://www.journalduhacker.net/rss) correspond à ce qui se trouve sur la page principale :  les articles les mieux notés, mélangés aux plus récents. Le paramétrage actuel du Jdh ne met en première page que les articles ayant eu au moins deux votes, ou un vote + un commentaire
* [Les articles récents](https://www.journalduhacker.net/newest.rss)  ne tient pas compte des meilleurs votes mais uniquement des dernières entrées (utile pour les modérateurs qui veulent suivre tout ce qui arrive en entrée)

Note : Il existe une subtile différence entre  :

* [Récents](https://www.journalduhacker.net/recent) : Infos avec un panaché aléatoire des infos récentes soumises qui n'ont pas encore atteint la page principale
* [Les plus récentes](https://www.journalduhacker.net/newest) : C'est l'ordre chronologique pur.

Mais le flux RSS de ces deux pages est le même  (https://www.journalduhacker.net/newest.rss) !

## <a name="marques"></a>Les flux des marques ##

Une marque, c'est le nom utilisé ici pour ce qu'on nomme couramment un _tag_ ailleurs (en franglais).
Chaque marque dispose de son propre flux RSS !

Vous pouvez les découvrir à partir de la page dédiée à chaque marque : Les marques elle mêmes sont regroupées dans la page [Filtres](https://www.journalduhacker.net/filters). Cliquez sur le nom d'une marque pour accéder à sa page.

![Page filtres](https://blog.journalduhacker.net/data/medias/filtres.png)

L'url d'une marque suit toujours le modèle suivant :

  * https://www.journalduhacker.net/t/_< nom de la marque>_

et son flux RSS:

   * https://www.journalduhacker.net/t/_< nom de la marque>_.__rss__

exemples :

* [Marque "android"](https://www.journalduhacker.net/t/android) : https://www.journalduhacker.net/t/android
* [Flux RSS de la marque "android"](https://www.journalduhacker.net/t/android.rss) https://www.journalduhacker.net/t/android.rss

* [Flux RSS de la marque "auto-hébergement"](https://www.journalduhacker.net/t/auto-h%C3%A9bergement.rss) https://www.journalduhacker.net/t/auto-h%C3%A9bergement.rss
* [Flux RSS de la marque "distributions linux"](https://www.journalduhacker.net/t/distributions%20linux.rss) https://www.journalduhacker.net/t/distributions%20linux.rss

Notez les échappements html pour les caractères accentués et les séparateurs  !

## <a name="ouimais"></a>Oui mais encore ? ##

A ma connaissance, il n'est pas possible de se créer directement son propre flux mixant plusieurs marques : il faut utiliser des outils externes pour cela.

Il est toutefois possible de :

1. filtrer les marques qui ne vous intéressent pas (page [Filtres](https://www.journalduhacker.net/filters))
2. à  partir d'une page générale ([Accueil]((https://www.journalduhacker.net) ou [Récents](https://www.journalduhacker.net/recent)), de récupérer l'url de votre flux personnalisé (merci Firefox)
3. le résultat devrait être votre flux personnalisé, constituté de toutes les marques sauf celles filtrées !

Cela nécessite donc que vous ayez un compte, pour définir vos filtres.  Si vous n'en avez pas, [réclamez une invitation](https://www.journalduhacker.net/invitations/request) sans attendre !
