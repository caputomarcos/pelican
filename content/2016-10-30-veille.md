Title: Compilation des liens de la semaine 2016-10-30
Date:  2016-10-30
Tags:  veille
Category:  bigdata
Compilation des [liens](https://tintouli.withknown.com/tag/bigdata) de la semaine

# Bases de données Graphe
[Enterprise Data Management with Graphs](https://neo4j.com/blog/enterprise-data-management-graphs)


# Spark
[Hydrator: Open Source, Code-Free Data Pipelines](https://www.infoq.com/presentations/hydrator)  _[#stream](https://tintouli.withknown.com/tag/stream)_    _[#hadoop](https://tintouli.withknown.com/tag/hadoop)_  


# Bases NoSQL
[Cassandra Data Modeling – Primary , Clustering , Partition , Compound Keys | Knoldus](https://blog.knoldus.com/2016/10/18/cassandra-data-modeling-primary-clustering-partition-compound-keys)  _[#cassandra](https://tintouli.withknown.com/tag/cassandra)_  

[Datanet: a New CRDT Database that Let's You Do Bad Bad Things to Distributed Data - High Scalability -](http://highscalability.com/blog/2016/10/17/datanet-a-new-crdt-database-that-lets-you-do-bad-bad-things.html)  _[#database](https://tintouli.withknown.com/tag/database)_  


# Streaming et Fast Data
[MariaDB MaxScale 2.0 Enables Faster Innovation for Secured Scale Out Environments - insideBIGDATA](http://insidebigdata.com/2016/10/29/mariadb-maxscale-2-0-enables-faster-innovation-for-secured-scale-out-environments)  _[#mariadb](https://tintouli.withknown.com/tag/mariadb)_  

[Released Mocked Streams for Apache Kafka](https://www.madewithtea.com/released-mocked-streams-for-apache-kafka.html)  _[#kafka](https://tintouli.withknown.com/tag/kafka)_    _[#scala](https://tintouli.withknown.com/tag/scala)_  

[Episode 29 : Apache Beam avec Jean-Baptiste Onofré | Bigdata Hebdo](http://www.spreaker.com/user/vhe74/episode-29-apache-beam-avec-jean-baptist)  _[#beam](https://tintouli.withknown.com/tag/beam)_    _[#podcast](https://tintouli.withknown.com/tag/podcast)_  

[Les Défis du Stream Processing et de l’Architecture Lambda](https://www.infoq.com/fr/news/2016/10/stream-processing-lambda)  _[#flink](https://tintouli.withknown.com/tag/flink)_    _[#kafka](https://tintouli.withknown.com/tag/kafka)_    _[#samza](https://tintouli.withknown.com/tag/samza)_  


# Machine Learning
[Machine Learning vs. Traditional Statistics: Different philosophies, Different Approaches - Data Science Central](http://www.datasciencecentral.com/profiles/blogs/machine-learning-vs-traditional-statistics-different-philosophi-1)

[Learn Data Science: Eight (Easy) Steps - insideBIGDATA](http://insidebigdata.com/2016/10/23/learndatascience_eight_easy_steps)  _[#infography](https://tintouli.withknown.com/tag/infography)_  

[How to Create Value From Raw Web Logs With Machine Learning - Dataiku](http://www.dataiku.com/blog/2016/10/20/machine-learning-web-logs-dataiku-dss.html)


# [Autres](https://tintouli.withknown.com/tag/others)
[Dataiku : 14 millions de dollars levés pour la plateforme de Data Science](http://www.lebigdata.fr/dataiku-levee-de-fonds-2510)

[China’s plan to organize its society relies on ‘big data’ to rate everyone - The Washington Post](https://www.washingtonpost.com/world/asia_pacific/chinas-plan-to-organize-its-whole-society-around-big-data-a-rating-for-everyone/2016/10/20/1cd0dd9c-9516-11e6-ae9d-0030ac1899cd_story.html)  _[#politics](https://tintouli.withknown.com/tag/politics)_  

[Big Data, algorithmes... "L'esprit porté par la Silicon Valley est totalitaire" - L'Express L'Expansion](http://lexpansion.lexpress.fr/high-tech/big-data-algorithmes-l-esprit-porte-par-la-silicon-valley-est-totalitaire_1841095.html)  _[#vision](https://tintouli.withknown.com/tag/vision)_  





----
_[Source d'inspiration](https://blog.jbfavre.org/2016/08/26/compilation-veille-twitter-rss/)_