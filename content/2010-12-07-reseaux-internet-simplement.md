date: 2010-12-07  
tags: lien, collegue, livre
title: Les réseaux, Internet, simplement  

Initialement intitulé *Internet pour les nuls* , une excellente
présentation du réseau et des protocoles, accessible à des non
techniciens :

[Les réseaux, Internet,
simplement](http://www.inlibroveritas.net/lire/oeuvre33335.html) , par
Nicolas Leroux

