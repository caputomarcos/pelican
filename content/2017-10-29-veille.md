Title: Compilation des liens de la semaine 2017-10-29
Date:  2017-10-29
Tags:  veille, spark, machinelearning, angular, docker
Category:  veille
Compilation des [liens](http://stream.logpickr.com/profile/daniel?_t=rss) de la semaine



# [Spark](http://stream.logpickr.com/profile/daniel?_t=rss/tag/spark)
Apache PredictionIO : L'apprentissage machine facile avec Spark - Le Monde Informatique
[http://www.lemondeinformatique.fr/actualites/lire-apache-predictionio-l-apprentissage-machine-facile-avec-spark-69825.html](http://www.lemondeinformatique.fr/actualites/lire-apache-predictionio-l-apprentissage-machine-facile-avec-spark-69825.html)  _[#machinelearning](http://stream.logpickr.com/profile/daniel?_t=rss/tag/machinelearning)_  







# [Docker](http://stream.logpickr.com/profile/daniel?_t=rss/tag/docker)
Angular in Docker with Nginx, supporting environments, using Docker multi-stage builds
[https://medium.com/@tiangolo/angular-in-docker-with-nginx-supporting-environments-built-with-multi-stage-docker-builds-bb9f1724e984](https://medium.com/@tiangolo/angular-in-docker-with-nginx-supporting-environments-built-with-multi-stage-docker-builds-bb9f1724e984)  _[#angular](http://stream.logpickr.com/profile/daniel?_t=rss/tag/angular)_  


# Autres
Why your Angular App is not Working: 11 common Mistakes
[https://malcoded.com/posts/why-angular-not-works](https://malcoded.com/posts/why-angular-not-works)  _[#angular](http://stream.logpickr.com/profile/daniel?_t=rss/tag/angular)_  

Best-practices learnt from delivering a quality Angular4 application
[https://hackernoon.com/best-practices-learnt-from-delivering-a-quality-angular4-application-2cd074ea53b3](https://hackernoon.com/best-practices-learnt-from-delivering-a-quality-angular4-application-2cd074ea53b3)  _[#angular](http://stream.logpickr.com/profile/daniel?_t=rss/tag/angular)_  





----
_[Source d'inspiration](https://blog.jbfavre.org/2016/08/26/compilation-veille-twitter-rss/)_