Title: Compilation des liens de la semaine 2016-11-06
Date:  2016-11-06
Tags:  veille, bigdata, nosql, mariadb, cassandra, mesos, spark, stream, flink, kafka, machinelearning, deeplearning
Category:  bigdata
Compilation des [liens](https://tintouli.withknown.com/tag/bigdata) de la semaine


# [Spark](https://tintouli.withknown.com/tag/spark)
[Apache Spark: A Unified Engine for Big Data Processing | November 2016 | Communications of the ACM](http://cacm.acm.org/magazines/2016/11/209116-apache-spark/fulltext)


# Bases NoSQL
[Getting to Know MariaDB ColumnStore | MariaDB](https://mariadb.com/blog/getting-know-mariadb-columnstore)  _[#mariadb](https://tintouli.withknown.com/tag/mariadb)_  

[How Uber Manages a Million Writes Per Second Using Mesos and Cassandra Across Multiple Datacenters  - High Scalability -](http://highscalability.com/blog/2016/9/28/how-uber-manages-a-million-writes-per-second-using-mesos-and.html)  _[#cassandra](https://tintouli.withknown.com/tag/cassandra)_    _[#mesos](https://tintouli.withknown.com/tag/mesos)_  


# Streaming et Fast Data
[Microservices and Stream Processing Architecture at Zalando Using Apache Flink](https://www.infoq.com/news/2016/10/zalando-stream-processing-flink)  _[#flink](https://tintouli.withknown.com/tag/flink)_    _[#kafka](https://tintouli.withknown.com/tag/kafka)_  


# Machine Learning
[Databricks enters deep learning market | Cloud Pro](http://www.cloudpro.co.uk/it-infrastructure/6419/databricks-enters-deep-learning-market)  _[#deeplearning](https://tintouli.withknown.com/tag/deeplearning)_  








----
_[Source d'inspiration](https://blog.jbfavre.org/2016/08/26/compilation-veille-twitter-rss/)_