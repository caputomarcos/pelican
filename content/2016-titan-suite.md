Title:  Titan DB, suite
Date: 2016-06-07
Tags:  graph, titandb
Category: bigdata
Slug: titan-db-suite

##Définition du modèle##

On peut définir les noms des relations entre noeuds, avec sa cardinalité, les propriétés, ...

Je l'interprète comme une possibilité pour la base d'optimiser les requêtes futures (traversée de graphe) sur la base des noms de relations entre noeuds.

###Noms des relations

```groovy
mgmt = graph.openManagement()
//Relations from Customer
holds = mgmt.makeEdgeLabel('holds').multiplicity(ONE2MANY).make()
isLocated = mgmt.makeEdgeLabel('isLocated').multiplicity(MULTI).make()
contracts = mgmt.makeEdgeLabel('contracts').multiplicity(ONE2MANY).make()
//Relations from Router
isConnectedTo = mgmt.makeEdgeLabel('isConnectedTo').multiplicity(MULTI).make()
isLoadbalancedBy = mgmt.makeEdgeLabel('isLoadbalancedBy').multiplicity(ONE2MANY).make()
isCascadedBy = mgmt.makeEdgeLabel('isCascadedBy').multiplicity(ONE2MANY).make()
isAssociatedTo = mgmt.makeEdgeLabel('isAssociatedTo').multiplicity(MANY2ONE).make()
//Relations from IS
isComposedOf = mgmt.makeEdgeLabel('isComposedOf').multiplicity(ONE2MANY).make()
isInstalledOn = mgmt.makeEdgeLabel('isInstalledOn').multiplicity(MANY2ONE).make()
dependsOn = mgmt.makeEdgeLabel('dependsOn').multiplicity(MULTI).make()
//Relations from SO
isSubscribedBy = mgmt.makeEdgeLabel('isSubscribedBy').multiplicity(MANY2ONE).make()
isProvisionedIn = mgmt.makeEdgeLabel('isProvisionedIn').multiplicity(MULTI).make()
isDeliveredOn = mgmt.makeEdgeLabel('isDeliveredOn').multiplicity(MANY2ONE).make()
isRelatedTo = mgmt.makeEdgeLabel('isRelatedTo').multiplicity(MANY2ONE).make()
isBilledTo = mgmt.makeEdgeLabel('isBilledTo').multiplicity(MANY2ONE).make()
contains = mgmt.makeEdgeLabel('contains').multiplicity(ONE2MANY).make()

mgmt.commit()
```

###Propriétés sur les noeuds###

```groovy
mgmt = graph.openManagement()
tacid = mgmt.makePropertyKey('tacid').dataType(String.class).cardinality(Cardinality.SINGLE).make()
name = mgmt.makePropertyKey('name').dataType(String.class).cardinality(Cardinality.SINGLE).make()
mgmt.commit()
```

Doit être unique dans le système :(pas de doublons entre différents labels ?).

Noms des propriétés et des relations sont des Relation Types ("Edge labels and property keys are jointly referred to as __relation types__")

Lister les noms des relations (edges)

```groovy
mgmt.getRelationTypes(EdgeLabel.class)
==>brother
==>composedOf
==>isCascading
==>father
==>mother
==>battled
==>lives
==>pet
```

Lister les nom des propriétés des noeuds (PropertyKey)

```groovy
mgmt.getRelationTypes(PropertyKey.class)
==>name
==>age
==>time
==>reason
==>place
```

###Labels des noeuds (~ types d'objets)

```groovy
mgmt = graph.openManagement()
router = mgmt.makeVertexLabel('Router').make()
site = mgmt.makeVertexLabel('Site').make()
customer = mgmt.makeVertexLabel('CustomerEntity').make()
is = mgmt.makeVertexLabel('InstalledService').make()
so = mgmt.makeVertexLabel('SubscribedOffer').make()
mgmt.commit()
```

### Création d'un noeud avec label

```groovy
tst1 = graph.addVertex(label, 'Router') //Automatically opens a new transaction
tst1.property("id", "tst1-id")
tst1.property("name", "this is my first test name")
graph.tx().commit()

// fichier
is = new File('.', 'router.csv').newDataInputStream()
is.readLine() // header
while (line = is.readLine()) {
cols = line.split(";")
if (g.V().has('name', cols[0])) { 
// router already declared
} else 
{ // new Router
r = graph.addVertex(label, 'Router')
r.property("name", cols[0])
r.property("tacid", "10-"+cols[0])
}
}
is.close()

is2 = new File('.','cust.csv').newDataInputStream()
is2.readLine()
while (line = is2.readLine()) {
cols = line.split('¤')
if (g.V().has('name', cols[111])) { 
} else 
{ 
c = graph.addVertex(label, 'CustomerEntity')
c.property("name", cols[111])
c.property("tacid", "10-"+cols[111])
}
         }
is2.close()


g.tx().commit()
# https://academy.datastax.com/resources/getting-started-tinkerpop-and-gremlin
getOrCreate = { l, id -> g.V().has('name', id).tryNext().orElseGet { tmp = g.addV(label, l , 'name', id).next(); tmp.property('tacid', "10-"+id); tmp}}

// indexation
mgmt = graph.openManagement()
name = mgmt.getPropertyKey('name')
mgmt.buildIndex('byNameComposite', Vertex.class).addKey(name).buildCompositeIndex()
mgmt.commit()
//Wait for the index to become available
mgmt.awaitGraphIndexStatus(graph, 'byNameComposite').call()
// Reindex existing objects
mgmt = graph.openManagement()
mgmt.updateIndex(mgmt.getGraphIndex('byNameComposite'), SchemaAction.REINDEX).get()
mgmt.commit()
```
